package ru.reboot.controller;


import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import ru.reboot.dao.AuthRepository;
import ru.reboot.dto.User;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.AuthServiceImpl;

import java.util.UUID;

public class AuthControllerImplTest {
    private final AuthControllerImpl authController;
    private final AuthServiceImpl authService;
    private final AuthRepository authRepositoryMock;

    {
        authRepositoryMock = Mockito.mock(AuthRepository.class);

        authService = new AuthServiceImpl();
        authService.setAuthRepository(authRepositoryMock);

        authController = new AuthControllerImpl();
        authController.setAuthService(authService);
    }


    @Test
    public void createUserTest() {
        String id = UUID.randomUUID().toString();
        User expectedUser = new User.Builder()
                .setUserId(id)
                .build();

        Mockito.when(authRepositoryMock.findUserByUserId(id))
                .thenReturn(null);
        Mockito.when(authRepositoryMock.createUser(expectedUser)).thenReturn(expectedUser);

        User actualUser = authController.createUser(expectedUser);
        Assertions.assertEquals(expectedUser, actualUser);

        Mockito.when(authRepositoryMock.findUserByUserId(id))
                .thenReturn(expectedUser);

        Executable executable = () -> authController.createUser(expectedUser);
        Assertions.assertThrows(BusinessLogicException.class, executable);
    }

    @Test
    public void updateUserTest() {
        String id = UUID.randomUUID().toString();
        User expectedUser = new User.Builder()
                .setUserId(id)
                .build();

        Mockito.when(authRepositoryMock.findUserByUserId(id))
                .thenReturn(expectedUser);
        Mockito.when(authRepositoryMock.updateUser(expectedUser)).thenReturn(expectedUser);

        User actualUser = authController.updateUser(expectedUser);
        Assertions.assertEquals(expectedUser, actualUser);

        Mockito.when(authRepositoryMock.findUserByUserId(id))
                .thenReturn(null);
        Executable executable = () -> authController.updateUser(expectedUser);
        Assertions.assertThrows(BusinessLogicException.class, executable);
    }

}
