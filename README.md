# Сервис auth-service

Данный сервис обеспечивает работу с авторизацией пользователей, которые используют SDiary.

Ответственный за сервис: Игорь Задубин

## План работ:

#### Создать таблицу user. Написать liquibase скрипт. Данная таблица хранит пользователей, которые зарегистрировалиь в SChat

Поля таблицы user

  * user_id: string
  * first_name: string
  * last_name: string
  * second_name: string
  * birth_date: LocalDate
  * login: string,
  * password: string,
  * telegram_chat_id,
  * roles: List<String>

### Создать соответствующий класс User.

### Реализовать rest-контроллер /auth/**/*, который обеспечивает работу с пользователями

План работ:

Игорь Задубин
- Написать ликубейс скрипты и разместить их в папке sql, написать структуру User, getters & setters + builder
- Получить информацию о пользователе GET /auth/user?userId={userId}
- Получить список пользователей с данными ролями GET /auth/user/all/byRoles

Александр Жуков
- Получить список всех пользователей GET /auth/user/all
- Удалить информацию о пользователе DELETE /auth/user?userId={userId}

Илья Семенов
- Создать нового пользователя POST /auth/user
- Редактировать существующего пользователя PUT /auth/user

Денис Вузов
- Привязать аккаунт телеграмма к пользователю PUT /auth/user/{userId}/telegram/attach?chatId={chatId}

Ринат Нагуманов
- Получить информацию о пользователе GET /auth/user?login={login}
- Получить информацио о пользователе GET /auth/user/byTelegramChat?chatId={chatId}

Критерии приемки:
- методы сервиса должны быть протестированы.